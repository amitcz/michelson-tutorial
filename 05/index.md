# Part V: Tez in Michelson

We've gone over how to write simple contracts in Michelson, originate them on
on the babylonnet and looked at some of the Michelson data structures. Now let's
take an in-depth look at `tez`, the official Tezos token.

## Terminology

In the Tezos whitepaper, the Tezos token is referred to as `tez` and `0.01 tez`
as one cent. Colloquially, Tezos tokens are referred to as `tezzy` and
`tezzie` in the singular and `tezzies` in the plural. These terms are not found
in the whitepaper, but are widely used in the community.

The whitepaper also recommends using the symbol `ꜩ` (unicode code point 
`\ua729`) to represent a `tez` tokens. The Tezos token exchange symbol is `XTZ`.
`100 tezzies`, `100 ꜩ` and `100 XTZ` are all common ways of writing 
`one-hundred tez` or `one-hundred tezzies`. We also hear people saying 
`one-hundred tezos` even though Tezos refers to the Tezos self-amending crypto 
ledger and the whitepaper makes no mention of a token called Tezos.

In the Michelson language you will find no reference to `tez` or `ꜩ`. Michelson 
uses a smaller denomination called `mutez`. One `tez` is equal to 1,000,000
`mutez` and one `mutez` is equal to 0.000001 `ꜩ`. Internally, `mutez` are 64-bit
signed integers, but you cannot create a negative amount of mutez. Mutez literals are
natural literals (0 or higher). Michelson prevents you from overflowwing mutez 
(making a value larger than possible) and from performing arithmetic operations 
on mutez with other numerical types. You can convert mutez to natural or 
integer, but you cannot convert natural or integer to mutez.

## Sending Tezos to a Contract

If you recall from the second tutorial, we originated a contract with the
following command.

```
$ ~/babylonnet.sh client originate contract idString \
                                   transferring 1 from alice \
                                   running container:idString.tz \
                                   --init '"hello"'
```

The sub command on the second line, `transferring 1 from alice`, means transfer 
one tez from `alice` to the `idString` contract. Now the tez is held by contract.
What is the point of putting the tez in a contract? In this case there is not a 
point. It is stuck in the contract, but you can design a contract such that when
certain conditions are met, it sends some amount of the tezos stored to another address.

If we want to call the contract and transfer more tezos, we can do the following:

```
$ ~/babylonnet.sh client transfer 2 from alice to idString --arg '"Good Evening"'
```

The `idString` contract should now have a balance of 3 tez. We can confirm this by running:

```
$ ~/babylonnet.sh client get balance for idString
3 ꜩ
```

## Useful Operations

Before we make a new contract, let's go over some useful operations that relate to the
tez stored in the contract.

### [Operations on contracts](https://tezos.gitlab.io/whitedoc/michelson.html#operations-on-contracts)

`AMOUNT` gives you the amount of mutez in the current transaction. In our 
example above, we had `transferring 1`, so amount would be
1000000 mutez. Remember that the unit for tezos client is tez and in Michelson
it is mutez. If we had sent one tezos cent, `transferring 0.01`, then `AMOUNT`
would return 10000 mutez. If we had sent 2.5 tez, then amount would return
2,500,000 mutez.

`BALANCE` gives you the current amount of tez stored in a contract. Right after
we originate the `idString` contract above, it has a balance of one tez or 
1,000,000 mutez. After the call we made to the contract transferring 2 tez, 
calling the `BALANCE` operation would return 3,000,000 mutez.

### [Operations on Mutez](https://tezos.gitlab.io/whitedoc/michelson.html#operations-on-mutez)

The rest are simple math operations directly on mutez, but there are a few 
details to watch out for.

`ADD` adds two mutez values and returns their sum. However, if the resulting
value exceeds 9,223,372,036,854,775,807 mutez or 
9,223,372,036,854.775807 tez, it will abort with an overflow error.

`SUB` subtracts a mutez value `y` from a mutez value `x` and returns the 
difference as a mutez value. If `x < y` then this operation will abort the
transaction because of an error. We know that `x` must be 0 or greater and
`y` must be 0 or greater. If `x` were less than `y`, it would return negative 
value.

`MUL` multiplies two mutez values and returns the result as a mutez value.
The same rules apply as `ADD`. If the result is greater than 
9,223,372,036,854,775,807 mutez, it will abort with an overflow error.

You can convert `nat` to `mutez` with the following code (assuming the top of 
the stack is `nat`).

```
PUSH nat 1;
MUL;        # the result of MUL nat mutez is mutez
```

`EDIV` divides `x` by `y` If it divides by zero, it 
will returns `None`, otherwise it returns `Some (Pair (x / y) (x % y))`
where `/` is integer division and `%` is modulus or the remainder of integer
division. There are two sets of types you can apply `EDIV` on. `mutez` and
`nat` which returns `option (pair mutez mutez)`, and `mutez` and `mutez`
which returns `option (pair nat mutez)`.

You can convert `mutez` to `nat` with the following code (assuming the top of 
the stack is `mutez`).

```
DIP { PUSH mutez 1; }; # put one below the mutez value we are converting
EDIV;                  # EDIV mutez mutez
IF_SOME { CAR; } { PUSH string "This shouldn't happend"; FAILWITH; }; # get the result of integer division, which is a nat
```

`COMPARE` compares two mutez value and returns an int of three possible 
values:

- iff `x < y` then `-1`
- iff `x = y` then `0`
- iff `x > y` then `1`

## Guess My Number Contract

Let's write a fun contract that involves two or more parties. The contract 
manager originates the contract and provides initial funds, the winnng pot, 
for the game. The participant(s) pays a fee that gets added to the winning pot
and guesses a number between 0 and 100. If the participant(s) guess the number
correctly, then they get the winning pot, if not, their funds are added to the
pot. If 15 guesses are made without any participant winning the pot, give the
winning pot to the contract manager.

For brevity, we will only write a simple contract without much error checking,
but we will discuss approaches to improving it in the Exercises.

```
parameter int; # the participant's guess
storage   (pair 
                int     # the number of guesses made by participants
                address # the address to send the winning pot to if the participants fail
          ); 
code {
       # (pair parameter storage) : []

       # make sure that the participant has contributed at least 1 tez
       PUSH mutez 1000000;
       AMOUNT;
       IFCMPGE {} { PUSH string "You did not provide enough tez."; FAILWITH; };
       
       # check that the number of guesses has not been exceeded
       UNPAIR; SWAP; # storage : parameter : []
       DUP;          # storage : storage : parameter : []
       CAR;          # int : storage : parameter : []
       DIP { PUSH int 15; };
       IFCMPLT { # check if guess is correct
                 SWAP; # parameter : storage : []
                 PUSH int 34;
                 IFCMPEQ { # the participant guessed correctly, give them the tokens.
                           SENDER;
                           CONTRACT unit; # convert the sender's address to a contract
                           IF_SOME {} { FAILWITH; };
                           BALANCE; # get the balance of the contract
                           UNIT;    # match the contract type
                           TRANSFER_TOKENS; # transfer the tokens to the winner
                           NIL operation; SWAP; CONS; PAIR;
                         }
                         { # the participant guessed incorrectly, increment the number of guesses performed.
                           UNPAIR;
                           PUSH int 1;
                           ADD;
                           PAIR;
                           NIL operation; PAIR;
                         };
               } 
               { # attempts exceeded, give winnings to the specified address
                 DIP { DROP; }; # storage : []
                 DUP; CDR;
                 CONTRACT unit; # convert the stored address to a contract
                 IF_SOME {} { FAILWITH; };
                 BALANCE;
                 UNIT;
                 TRANSFER_TOKENS; # transfer the tokens to the contract manager
                 NIL operation; SWAP; CONS; PAIR;
               };
     };
```

### Playing the game

First type check the game contract.

```
~/babylonnet.sh client typecheck script container:game.tz
```

I made a new account to be the manager which I called simon. You can use
any account you like. If you forget how to make one, look back at the second 
tutorial. Before you originate the contract you will need the address of the
manager.

```
~/babylonnet.sh client list known addresses
```

Now let's originate the game contract.

```
~/babylonnet.sh client originate contract game \
                                 transferring 10 from simon \
                                 running container:game.tz \
                                 --init '(Pair 0 "tz1cSBeSxJf6dK1U8HtMSvPt2hMSYDtbf4JB")' --burn-cap 1
```

Now we can start playing by transferring at least one tez and our guess.

```
~/babylonnet.sh client transfer 1 from alice to game2 --arg '2' --burn-cap 1
~/babylonnet.sh client transfer 1 from bob to game2 --arg '8' --burn-cap 1
```

You can check the balanceof the contract and its storage at any time.

```
~/babylonnet.sh client get balance for game
~/babylonnet.sh client get script storage for game
```

Keep playing until someone guesses the correct number or after 15 attempts, then
check the balance of the winner and the contract.

## Conclusion

You should now have a good understanding of how to tokens work in Michelson 
smart contracts. 

## Exercises

There are many security flaws and ways we can improve the game contract.

1. Do not allow the contract manager to act as a participant in the game.
   (Hint: compare the stored address with the sender address).

2. The range of the guessed number is supposed to be 0 to 100. Check that the
   participant's guess is within this range. If not, their attempt should fail
   and the number of guesses should not increment.

3. Allow the number of attempts to be set by the contract manager at 
   origination. (Hint: you need to change the storage).

4. Currently the game can only be played once. Think about how to make
   it replayable.

5. The biggest flaw is that the answer is hardcoded in the game. The 
   participants could read the source code and get the answer. Is there a way
   the contract can hide the value? (How to implement is beyond the scope of
   this lesson, we will approach that in later, but try to come up with a 
   conceptual way of hiding the value.)

# Appendix A: Tezos Tokens Terminology

| Terminology                     | Meaning   |
|---------------------------------|-----------|
| `tez`                           | The unit of the official Tezos token |
| `ꜩ`                             | A unicode symbol for `tez`           |
| `XTZ`                           | The exchange symbol for `tez`        |
| `one tez cent`                 | 1/100th of a `tez` or 0.01 `tez`    |
| `tezzy`, `tezzie`, `tezzies`  | Colloquial terms for `tez`          |
| `mutez`                         | 1/1000000th of a `tez` and a type in Michelson that represents |


# Appendix B: Mutez Functions in Michelson

| Operation          | Explanation                                                                        |
|--------------------|-------------------------------------------------------------------------------------|
| `ADD`              | addition that fails with overflow                                                  |
| `SUB`              | subtraction that fails if result is negative                                       |
| `MUL`              | multiplication that fails with overflow                                            |
| `EDIV`             | division that returns integer division and modulus result                          |
| `COMPARE`          | compare two mutez values, returns -1, 0 or 1                                       |
| `TRANSFER_TOKENS` | send tokens to account/contract, can also be used to call a contract in a contract |
| `BALANCE`          | the current amount of mutez held in a contract                                     |
| `AMOUNT`           | the amount of of mutez in the current transaction, sent by the sender              |
